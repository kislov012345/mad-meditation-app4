package com.example.madmeditationapp

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.EditText
import android.widget.Toast
import com.example.madmeditationapp.data.api.MyRetrofit
import com.example.madmeditationapp.data.model.LoginResponse
import com.example.madmeditationapp.ui.profile.ProfileFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern.compile

class SignInActivity : AppCompatActivity() {
    lateinit var email:EditText
    lateinit var password:EditText
    lateinit var pattern: String
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)

        sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)

        pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,50}" +
                "\\@" +
                "[a-zA-Z][a-zA-Z]{0,8}" +
                "(" +
                "\\." +
                "[a-zA-Z][a-zA-Z0-9\\-]{0,5}" +
                ")+"
    }
    fun EmailValid(em:String): Boolean{return compile(pattern).matcher(em).matches()}
    fun reg2(view: android.view.View){
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
    }
    fun signin(view: android.view.View){
        val emailText = email.text.toString()
        val passwordText = password.text.toString()

        if(emailText.isNotEmpty() && passwordText.isNotEmpty()){
            if (EmailValid(emailText)){
                val hashMap = hashMapOf<String,String>()
                hashMap.put("email", emailText)
                hashMap.put("password", passwordText)
                val retrofit = MyRetrofit.getRetrofit().getAuth(hashMap = hashMap)

                retrofit.enqueue(object:retrofit2.Callback<LoginResponse>{
                    override fun onResponse(
                        call: Call<LoginResponse>,
                        response: Response<LoginResponse>
                    ) {
                        if (response.isSuccessful){
                            val editor = sharedPreferences.edit()
                            editor.putString("avatar", response.body()?.avatar)
                            editor.putString("token", response.body()?.token)
                            editor.putString("nickName", response.body()?.nickName)
                            editor.putString("email", response.body()?.email)
                            editor.apply()



                            val menu = Intent(this@SignInActivity, MenuActivity::class.java)
                            startActivity(menu)
                        }
                        else{
                            Toast.makeText(this@SignInActivity, response.errorBody()?.toString(), Toast.LENGTH_SHORT).show()

                        }

                    }

                    override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                        Toast.makeText(this@SignInActivity, t.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                })








            }
            else{
                val alert = AlertDialog.Builder(this)
                    .setTitle("Ошибка входа")
                    .setMessage("Ошибка email")
                    .setPositiveButton("Ok", null)
                    .create()
                    .show()

            }

        }
        else{
            val alert = AlertDialog.Builder(this)
                .setTitle("Ошибка входа")
                .setMessage("У Вас есть пустые поля")
                .setPositiveButton("Ok", null)
                .create()
                .show()
            Toast.makeText(this, "У Вас есть пустые поля", Toast.LENGTH_SHORT).show()
        }

    }
    fun profile(view: android.view.View){
        val prof = Intent(this, ProfileFragment::class.java)
        startActivity(prof)

    }
}

private fun <T> Call<T>.enqueue(callback: Callback<T>, function: () -> Unit) {

}
