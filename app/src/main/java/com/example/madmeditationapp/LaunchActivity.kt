package com.example.madmeditationapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class LaunchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        val sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)
        val timer = object : CountDownTimer(10000, 200)
        {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                val token = sharedPreferences.getString("token",null)
                if (token != null){
                    val intent = Intent(this@LaunchActivity, MenuActivity::class.java)
                    startActivity(intent)

                }
                else{
                    val intent = Intent(this@LaunchActivity, OnboardingActivity::class.java)
                    startActivity(intent)

                }


                finish()

            }

        }
        timer.start()

    }
}