package com.example.madmeditationapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.madmeditationapp.R
import com.example.madmeditationapp.data.model.Feel

class FeelAdapter(val context: Context, val list: ArrayList<Feel>): RecyclerView.Adapter<FeelAdapter.MyVH>() {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView){
        val image:ImageView = itemView.findViewById(R.id.image_feel)
        val text2:TextView = itemView.findViewById(R.id.text_feel)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeelAdapter.MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.feel_adapter,parent, false)
        return MyVH(root)

    }
    override fun onBindViewHolder(holder: FeelAdapter.MyVH, position: Int) {
        Glide.with(context).load(list[position].image).into(holder.image)

        holder.text2.setText(list[position].title)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}