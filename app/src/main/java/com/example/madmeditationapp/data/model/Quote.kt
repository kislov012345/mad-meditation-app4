package com.example.madmeditationapp.data.model

data class Quote(
    val id: Int,
    val title: String,
    val image: String,
    val description: String
)
