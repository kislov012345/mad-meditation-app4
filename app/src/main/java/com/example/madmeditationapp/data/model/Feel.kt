package com.example.madmeditationapp.data.model

data class Feel(
    val id: Int,
    val title: String,
    val image: String
)
