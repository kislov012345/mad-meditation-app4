package com.example.madmeditationapp.data.api

import com.example.madmeditationapp.data.model.FeelingsResponse
import com.example.madmeditationapp.data.model.LoginResponse
import com.example.madmeditationapp.data.model.QuotesResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiRetrofit {

    @POST("user/login")
    fun getAuth(
        @Header("Content-type") type: String = "application/json",
        @Body hashMap: HashMap<String,String>
    ):Call<LoginResponse>

    @GET("quotes")
    fun getQuotes():Call<QuotesResponse>

    @GET("feelings")
    fun getFeelings():Call<FeelingsResponse>


}