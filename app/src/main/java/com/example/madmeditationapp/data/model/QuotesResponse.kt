package com.example.madmeditationapp.data.model

data class QuotesResponse(
    val success: Boolean,
    val data: ArrayList<Quote>
)

