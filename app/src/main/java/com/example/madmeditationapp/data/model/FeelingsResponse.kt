package com.example.madmeditationapp.data.model

data class FeelingsResponse(
    val success: Boolean,
    val data: ArrayList<Feel>
)
