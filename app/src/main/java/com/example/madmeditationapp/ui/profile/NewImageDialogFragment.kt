package com.example.madmeditationapp.ui.profile

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.example.madmeditationapp.R

class NewImageDialogFragment:DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val root = LayoutInflater.from(activity).inflate(R.layout.new_image_dialog, null)
        val camera : Button = root.findViewById(R.id.camera_button)
        val gallery : Button = root.findViewById(R.id.gallery_button)

        gallery.setOnClickListener{
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            activity?.startActivityForResult(intent,1)
            dialog?.cancel()

        }
        camera.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            activity?.startActivityForResult(intent,2)
            dialog?.cancel()

        }
        builder.setView(root)
        return builder.create()

    }
}