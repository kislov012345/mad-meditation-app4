package com.example.madmeditationapp.ui.profile

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.madmeditationapp.NewImageClickListener
import com.example.madmeditationapp.R
import com.example.madmeditationapp.SignInActivity
import com.example.madmeditationapp.adapters.ImagesAdapter

class ProfileFragment : Fragment(R.layout.fragment_profile), NewImageClickListener {
    val list:ArrayList<Int> = arrayListOf(
        R.drawable.maxresdefault,
        R.drawable.image2,
        R.drawable.image1,
        R.drawable.icon
    )


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button: Button = view.findViewById(R.id.exit)
        val recyclerview: RecyclerView = view.findViewById(R.id.images_recyclerview)
        recyclerview.adapter = ImagesAdapter(requireContext(),list, this)
        val sharedPreferences:SharedPreferences? = activity?.getSharedPreferences("main", MODE_PRIVATE)
        button.setOnClickListener {
            if (sharedPreferences != null){
                val editor = sharedPreferences.edit()
                editor.putString("token", null)
                editor.apply()


            }
            val intent = Intent(requireContext(), SignInActivity::class.java)
            activity?.startActivity(intent)


        }
    }


    override fun NewImage() {
        val dialog = NewImageDialogFragment()
        val fm = fragmentManager
        if (fm != null){
            dialog.show(fm, "123")
        }

    }
}