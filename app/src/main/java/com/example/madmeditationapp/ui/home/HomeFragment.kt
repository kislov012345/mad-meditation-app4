package com.example.madmeditationapp.ui.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.madmeditationapp.R
import com.example.madmeditationapp.adapters.FeelAdapter
import com.example.madmeditationapp.adapters.StateAdapter
import com.example.madmeditationapp.data.api.MyRetrofit
import com.example.madmeditationapp.data.model.FeelingsResponse
import com.example.madmeditationapp.data.model.QuotesResponse
import com.example.madmeditationapp.databinding.FragmentHomeBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment() {


    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val avatar: ImageView = root.findViewById(R.id.avatar)
        val helloText: TextView = root.findViewById(R.id.helloText)
        val sharedPreferences:SharedPreferences? = activity?.getSharedPreferences("main", Context.MODE_PRIVATE)
        val recyclerView: RecyclerView = root.findViewById(R.id.feel)
        val recyclerView2: RecyclerView = root.findViewById(R.id.state)
        Glide.with(requireContext()).load(sharedPreferences?.getString("avatar","")).circleCrop().into(avatar)
        val name = sharedPreferences?.getString("nickName", "Default")
        helloText.text = "С возвращением, $name"

        val retrofit = MyRetrofit.getRetrofit().getQuotes()
        retrofit.enqueue(object : Callback<QuotesResponse>{
            override fun onResponse(
                call: Call<QuotesResponse>,
                response: Response<QuotesResponse>
            ) {
                if (response.isSuccessful){
                    recyclerView2.adapter =
                        response.body()?.data?.let { StateAdapter(requireContext(), it) }
                }

            }


            override fun onFailure(call: Call<QuotesResponse>, t: Throwable) {
              Toast.makeText(requireContext(), t.localizedMessage, Toast.LENGTH_SHORT).show()
            }

        })

        val retrofit2 = MyRetrofit.getRetrofit().getFeelings()
        retrofit2.enqueue(object : Callback<FeelingsResponse>{
            override fun onResponse(
                call: Call<FeelingsResponse>,
                response: Response<FeelingsResponse>
            ) {
                if (response.isSuccessful){
                    recyclerView.adapter =
                        response.body()?.data?.let { FeelAdapter(requireContext(), it) }

                }

            }

            override fun onFailure(call: Call<FeelingsResponse>, t: Throwable) {
                Toast.makeText(requireContext(), t.localizedMessage, Toast.LENGTH_SHORT).show()

            }

        })






        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}